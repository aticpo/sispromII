CREATE schema if NOT EXISTS bd_sispromii; 

CREATE TABLE tb_login (
  id_login SERIAL PRIMARY KEY,
  tx_login VARCHAR(10) NOT NULL,
  hash_senha VARCHAR(100) NOT NULL
);
