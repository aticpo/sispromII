package intraer.cpo.sispromii.restrict.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tb_login")
public class LoginAcesso implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_login")
	private Long idLogin;
	
	@NotNull
	@Size(min = 7, max = 10)
	@Column(name = "tx_login", unique = true, length = 10)
	private String txLogin;
	
	@NotNull
	@Column(name = "hashSenha", length = 100)
	private String hash_senha;

	public Long getIdLogin() {
		return idLogin;
	}

	public void setIdLogin(Long idLogin) {
		this.idLogin = idLogin;
	}

	public String getTxLogin() {
		return txLogin;
	}

	public void setTxLogin(String txLogin) {
		this.txLogin = txLogin;
	}

	public String getHash_senha() {
		return hash_senha;
	}

	public void setHash_senha(String hash_senha) {
		this.hash_senha = hash_senha;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
