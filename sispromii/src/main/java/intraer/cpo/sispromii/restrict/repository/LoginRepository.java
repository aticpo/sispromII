package intraer.cpo.sispromii.restrict.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import intraer.cpo.sispromii.restrict.entities.LoginAcesso;

public interface LoginRepository extends JpaRepository<LoginAcesso, Long> {

}
