package intraer.cpo.sispromii.restrict.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import intraer.cpo.sispromii.event.EventCreate;
import intraer.cpo.sispromii.restrict.entities.LoginAcesso;
import intraer.cpo.sispromii.restrict.repository.LoginRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("/restrict")
public class LoginController {

	@Autowired
	private LoginRepository loginRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping("/getLogins")
	public 	List<LoginAcesso> getAllLogins(){
		return loginRepository.findAll();
	}
	
	@GetMapping("/getLoginId/{codigo}")
	public ResponseEntity<LoginAcesso> buscarPeloCodigo(@PathVariable Long codigo) {
	return this.loginRepository.findById(codigo)
	  .map(loginRepository -> ResponseEntity.ok(loginRepository))
	  .orElse(ResponseEntity.notFound().build()); //Retorna erro 404 NotFound caso não encontre o ID
	}
	
	@PostMapping("/postLogin")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<LoginAcesso> setLogin(@Valid @RequestBody LoginAcesso loginAcesso, HttpServletResponse res) {
		LoginAcesso newLoginAcesso = loginRepository.save(loginAcesso);
		publisher.publishEvent(new EventCreate(this, res, newLoginAcesso.getIdLogin()));
		return ResponseEntity.status(HttpStatus.CREATED).body(newLoginAcesso);
	}
	
	@DeleteMapping("/delLogin/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removerLogin(@PathVariable Long codigo ){
		this.loginRepository.deleteById(codigo);
	}

}
