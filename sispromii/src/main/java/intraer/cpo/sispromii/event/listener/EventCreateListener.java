package intraer.cpo.sispromii.event.listener;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import intraer.cpo.sispromii.event.EventCreate;

@Component
public class EventCreateListener implements ApplicationListener<EventCreate>{

	@Override
	public void onApplicationEvent(EventCreate event) {
		HttpServletResponse res = event.getRes();
		Long id = event.getId();
		addHeaderLocation(res, id);		
	}

	private void addHeaderLocation(HttpServletResponse res, Long id) {
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}")
				.buildAndExpand(id).toUri();
		res.setHeader("Location", uri.toASCIIString());
	}
	
	

}
