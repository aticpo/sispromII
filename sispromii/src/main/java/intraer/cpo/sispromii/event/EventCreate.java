package intraer.cpo.sispromii.event;

import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationEvent;

public class EventCreate extends ApplicationEvent{

	private static final long serialVersionUID = 1L;
	
	private HttpServletResponse res;
	private Long id;
	
	public EventCreate (Object source, HttpServletResponse res, Long id) {
		super(source);
		this.res = res;
		this.id = id;
	}

	public HttpServletResponse getRes() {
		return res;
	}
	public Long getId() {
		return id;
	}	
}
