package intraer.cpo.sispromii;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;


@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "SISPROM II - API", version = "1.0", description = "API Sistema de Promoções - FAB/CPO<br/><b>Contato:</b> (61) 9821-04151 Ten QOCON ANS Fernando."))
public class SispromiiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SispromiiApplication.class, args);
	}

}
