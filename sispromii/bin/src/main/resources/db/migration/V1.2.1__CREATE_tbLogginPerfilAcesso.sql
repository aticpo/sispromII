CREATE TABLE ta_login_perfil_acesso (
  fk_id_login SERIAL NOT NULL,
  fk_id_perfil_acesso SERIAL NOT NULL,
  
  PRIMARY KEY(fk_id_login, fk_id_perfil_acesso),

  CONSTRAINT fk_login_has_perfil_acesso FOREIGN KEY(fk_id_login) REFERENCES tb_login(id_login),
  CONSTRAINT fk_perfil_acesso_has_login FOREIGN KEY(fk_id_perfil_acesso) REFERENCES tb_perfil_acesso(id_perfil_acesso)
);